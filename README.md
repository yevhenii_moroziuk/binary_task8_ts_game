# Typescript

Клонировать [готовий стартер](https://github.com/binary-studio-academy/stage-2-es6-for-everyone). А дальше:

1.  В классе FighterService создать метод для получения детальной информации про бойца. Добавить возможность ее просмотра — для этого реализовать метод, который бы обрабатывал клик по бойцу и, если информации про него еще нет на клиенте, то отправить запрос и записати информацию в коллекцию. Данные можно отображать в виде небольшого диалогового окна с возможностью их редактирования.
    
2.  Создать класс Fighter, который будет содержать информацию, необходимую бойцу для боя (name, health, attack, defense, etc.), а также методы:
    

-   getHitPower, который бы рассчитывал силу удара (количество причиненного урона здоровью противника) по формуле power = attack * criticalHitChance, где criticalHitChance — случайное число от 1 до 2,
-   getBlockPower, который бы рассчитывал силу блока (аммортизация удара противника) по формуле power = defense * dodgeChance, где dodgeChance — случайное число от 1 до 2.

3.  Создать функцию fight, которая будет принимать бойцов, как параметры и запускать процесс игры. Игроки наносят удары друг другу, а уровень их health уменьшается на getHitPower - getBlockPower (или же 0, если боец "уклонился" от удара полностью). Если один из них умирает, то игра заканчивается и имя победителя выводиться на экран. (Ход боя может быть произвольным— например, бойцы могут наносить удары по очереди или с некоторым интервалом, или же могут это делать с помощью определенного UI элемента на странице).
    
4.  Создать дополнительные элементы для реализации игры — элемент для выбора бойцов для боя (может быть обычный checkbox), кнопка, которая начинает бой, индикатор здоровья, и тд.
    
5.  Все файлы должны иметь расширение .ts.Все переменные, аргументы, возвращаемые значения должны быть помечены соответствующим типом (если typescript сам не может их вывести).
    

**Здаания необходимо выполнять в Bitbucket репозитории (не Github)**