import { callApi } from '../helpers/apiHelper';
import { Fighter } from '../models/Fighter';
import { Fighterlite } from '../models/Fighterlite';

class FighterService {
  async getFighters():Promise<Array<Fighterlite>> {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }

    async getFighterDetails(_id: number):Promise<Fighter>{
    // implement this method
      const endpoint = `details/fighter/${_id}.json`;
      let fighter: Fighter;

        try {
          const apiResult = await callApi(endpoint, 'GET');
          fighter = JSON.parse(atob(apiResult.content));
          return fighter;
      } catch (error) {
          throw error;
      }

  }
}

export const fighterService = new FighterService();
