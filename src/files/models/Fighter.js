"use strict";
exports.__esModule = true;
var Fighter = /** @class */ (function () {
    function Fighter(_id, name, attack, defense, health, source) {
        this._id = _id;
        this.name = name;
        this.attack = attack;
        this.defense = defense;
        this.health = health;
        this.source = source;
    }
    Fighter.prototype.getHitPower = function () {
        return this.attack * this.getRandomNumber();
    };
    Fighter.prototype.getBlockPower = function () {
        return this.defense * this.getRandomNumber();
    };
    Fighter.prototype.getRandomNumber = function () {
        return Math.floor(Math.random() * 2 + 1);
    };
    return Fighter;
}());
exports.Fighter = Fighter;
