
export interface IFighter {
    _id: number;
    name: string;
    health: number;
    attack: number;
    defense: number;
    source:string;

    getHitPower(): number;
    getBlockPower(): number;
    getRandomNumber():number;
}

export class Fighter implements IFighter {
    _id: number;
    name: string;
    health: number;
    attack: number;
    defense: number;
    source:string;

    constructor(_id:number, name:string, attack:number, defense:number, health:number, source:string) {
        this._id = _id;
        this.name = name;
        this.attack = attack;
        this.defense = defense;
        this.health = health;
        this.source = source;
    }

    public getHitPower(): number {
        return this.attack * this.getRandomNumber();
    }
    public getBlockPower(): number {
        return this.defense * this.getRandomNumber();
    }

    getRandomNumber(): number {
        return Math.floor(Math.random() * 2 + 1);
    }

}