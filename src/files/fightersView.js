"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var view_1 = require("./view");
var fighterView_1 = require("./fighterView");
var fightersService_1 = require("./services/fightersService");
var Fighter_1 = require("./models/Fighter");
var fighterCheckView_1 = require("./fighterCheckView");
var FightersView = /** @class */ (function (_super) {
    __extends(FightersView, _super);
    function FightersView(fighters) {
        var _this = _super.call(this) || this;
        _this.fightersDetailsMap = new Map();
        _this.handleClick = _this.handleFighterClick.bind(_this);
        _this.createFighters(fighters);
        document.getElementById('update-fighter').addEventListener('click', function (event) { return _this.handleClickUpdate(event); }, false);
        _this.handleClickUpdate = _this.editFighter.bind(_this);
        document.getElementById('start').addEventListener('click', function (event) { return _this.handleClickStart(event); });
        _this.handleClickStart = _this.fight.bind(_this);
        return _this;
    }
    FightersView.prototype.createFighters = function (fighters) {
        var _a;
        var _this = this;
        var fighterElements = fighters.map(function (fighter) {
            var fighterView = new fighterView_1["default"](fighter, _this.handleClick);
            return fighterView.element;
        });
        var fighterCheckElements = new fighterCheckView_1["default"](fighters).element;
        this.element = this.createElement({ tagName: 'div', className: 'fighters' });
        (_a = this.element).append.apply(_a, fighterElements);
        this.element.append(fighterCheckElements);
    };
    FightersView.prototype.handleFighterClick = function (event, fighter) {
        return __awaiter(this, void 0, void 0, function () {
            var card, t_fighter, fighter_id, fighter_name, fighter_source, fighter_attack, fighter_defense, fighter_health;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        card = document.getElementById("fighter-card-info");
                        card.style.display = "block";
                        console.log(fighter);
                        if (!(this.fightersDetailsMap.get(fighter._id) == null)) return [3 /*break*/, 2];
                        return [4 /*yield*/, fightersService_1.fighterService.getFighterDetails(+fighter._id)];
                    case 1:
                        t_fighter = _a.sent();
                        this.fightersDetailsMap.set(t_fighter._id, t_fighter);
                        return [3 /*break*/, 3];
                    case 2:
                        t_fighter = this.fightersDetailsMap.get(fighter._id);
                        _a.label = 3;
                    case 3:
                        fighter_id = document.getElementById("fighter_id");
                        fighter_name = document.getElementById("fighter_name");
                        fighter_source = document.getElementById("fighter_source");
                        fighter_attack = document.getElementById("fighter_attack");
                        fighter_defense = document.getElementById("fighter_defense");
                        fighter_health = document.getElementById("fighter_health");
                        fighter_id.value = t_fighter._id.toString();
                        fighter_name.value = t_fighter.name;
                        fighter_source.value = fighter.source;
                        fighter_attack.value = t_fighter.attack.toString();
                        fighter_defense.value = t_fighter.defense.toString();
                        fighter_health.value = t_fighter.health.toString();
                        return [2 /*return*/];
                }
            });
        });
    };
    FightersView.prototype.editFighter = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var card, fighter_id, fighter_name, fighter_source, fighter_attack, fighter_defense, fighter_health, t_fighter, id;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        card = document.getElementById("fighter-card-info");
                        fighter_id = document.getElementById("fighter_id");
                        fighter_name = document.getElementById("fighter_name");
                        fighter_source = document.getElementById("fighter_source");
                        fighter_attack = document.getElementById("fighter_attack");
                        fighter_defense = document.getElementById("fighter_defense");
                        fighter_health = document.getElementById("fighter_health");
                        card.style.display = "none";
                        id = +fighter_id.value;
                        if (!(this.fightersDetailsMap.get(id.toString()) == null)) return [3 /*break*/, 2];
                        return [4 /*yield*/, fightersService_1.fighterService.getFighterDetails(id)];
                    case 1:
                        t_fighter = _a.sent();
                        this.fightersDetailsMap.set(t_fighter._id.toString(), t_fighter);
                        return [3 /*break*/, 3];
                    case 2:
                        t_fighter = this.fightersDetailsMap.get(id.toString());
                        _a.label = 3;
                    case 3:
                        console.log(t_fighter._id);
                        t_fighter.name = fighter_name.value;
                        t_fighter.health = +fighter_health.value;
                        t_fighter.attack = +fighter_attack.value;
                        t_fighter.defense = +fighter_defense.value;
                        t_fighter.source = fighter_source.value;
                        console.log(t_fighter);
                        this.fightersDetailsMap.set(t_fighter._id.toString(), t_fighter);
                        return [2 /*return*/];
                }
            });
        });
    };
    FightersView.prototype.fight = function (event) {
        return __awaiter(this, void 0, void 0, function () {
            var checkedCheckbox, allInputs, i, max, ft1, ft2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        checkedCheckbox = [];
                        allInputs = document.getElementsByTagName("input");
                        for (i = 0, max = allInputs.length; i < max; i++) {
                            if (allInputs[i].type === 'checkbox')
                                if (allInputs[i].checked)
                                    checkedCheckbox.push(+allInputs[i].id);
                        }
                        if (!(checkedCheckbox.length != 2)) return [3 /*break*/, 1];
                        alert('Select exactly 2 fighters!');
                        return [3 /*break*/, 6];
                    case 1:
                        ft1 = this.fightersDetailsMap.get(checkedCheckbox[0].toString());
                        ft2 = this.fightersDetailsMap.get(checkedCheckbox[1].toString());
                        if (!(ft1 == undefined)) return [3 /*break*/, 3];
                        return [4 /*yield*/, fightersService_1.fighterService.getFighterDetails(checkedCheckbox[0])];
                    case 2:
                        ft1 = _a.sent();
                        _a.label = 3;
                    case 3:
                        if (!(ft2 == undefined)) return [3 /*break*/, 5];
                        return [4 /*yield*/, fightersService_1.fighterService.getFighterDetails(checkedCheckbox[1])];
                    case 4:
                        ft2 = _a.sent();
                        _a.label = 5;
                    case 5:
                        this.performFight(ft1, ft2);
                        _a.label = 6;
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    FightersView.prototype.performFight = function (fighter1, fighter2) {
        console.log(fighter1.health);
        console.log(fighter2.health);
        //doesn`t work without this manipulation)
        var ft1 = new Fighter_1.Fighter(fighter1._id, fighter1.name, fighter1.attack, fighter1.defense, fighter1.health, fighter1.source);
        var ft2 = new Fighter_1.Fighter(fighter2._id, fighter2.name, fighter2.attack, fighter2.defense, fighter2.health, fighter2.source);
        //
        //hear will be code to display game screen above main
        var game_btn_start = document.getElementById('start');
        game_btn_start.style.display = 'none';
        var game_field = document.getElementById('game-screen');
        game_field.style.display = 'block';
        document.getElementById('f-1-f').appendChild(this.CreateFighterPicture(ft1.source, 'f-img-1'));
        document.getElementById('f-2-f').appendChild(this.CreateFighterPicture(ft2.source, 'f-img-2'));
        //
        function interval() {
            if (ft1.health <= 0 || ft2.health <= 0) {
                if (ft1.health <= 0)
                    alert(ft2.name + " won");
                else
                    alert(ft1.name + " won");
                console.log('finished');
                clearInterval(intervalID);
                game_field.style.display = 'none';
                game_btn_start.style.display = 'block';
                document.getElementById('f-1-f').removeChild(document.getElementById('f-1-f').firstChild);
                document.getElementById('f-2-f').removeChild(document.getElementById('f-2-f').firstChild);
                document.getElementById('f-1').innerHTML = '';
                document.getElementById('f-2').innerHTML = '';
            }
            else {
                var kick1 = ft2.getHitPower() - ft1.getBlockPower();
                var kick2 = ft1.getHitPower() - ft2.getBlockPower();
                if (kick1 <= 0)
                    ft1.health = ft1.health;
                else
                    ft1.health = ft1.health - kick1;
                if (kick2 <= 0)
                    ft1.health = ft1.health;
                else
                    ft2.health = ft2.health - kick2;
                //
                document.getElementById('f-1').innerHTML = "<h1>" + ft1.name + ": " + ft1.health.toString() + "</h1>";
                document.getElementById('f-2').innerHTML = "<h1>" + ft2.name + ": " + ft2.health.toString() + "</h1>";
                //
                console.log(ft1.name + ": " + ft1.health + "; " + ft2.name + ": " + ft2.health);
            }
        }
        var intervalID = setInterval(interval, 700);
    };
    FightersView.prototype.CreateFighterPicture = function (source, className) {
        var elem = document.createElement("img");
        elem.setAttribute("src", source);
        elem.className = className;
        return elem;
    };
    return FightersView;
}(view_1["default"]));
exports["default"] = FightersView;
