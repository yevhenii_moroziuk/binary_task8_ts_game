import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import { Fighter } from './models/Fighter';
import { Fighterlite } from './models/Fighterlite';

import FighterCheckView from './fighterCheckView';

class FightersView extends View {
    handleClick: EventHandlerNonNull;
    handleClickUpdate:EventHandlerNonNull;
    handleClickStart:EventHandlerNonNull;

    constructor(fighters:Array<Fighterlite>) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);

    document.getElementById('update-fighter').addEventListener('click', event => this.handleClickUpdate(event), false);
    this.handleClickUpdate = this.editFighter.bind(this);

    document.getElementById('start').addEventListener('click', event => this.handleClickStart(event));
    this.handleClickStart = this.fight.bind(this);
    
  }
  fightersDetailsMap =  new Map<string, Fighter>();

  createFighters(fighters:Array<Fighterlite>) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });
   
    const fighterCheckElements =  new FighterCheckView(fighters).element;
    
    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
    this.element.append(fighterCheckElements);
  }

  async handleFighterClick(event, fighter:Fighterlite) {

      let card = document.getElementById("fighter-card-info");
      card.style.display = "block";
      console.log(fighter)
      let t_fighter;

      if(this.fightersDetailsMap.get(fighter._id)==null)
        {
          t_fighter = await fighterService.getFighterDetails(+fighter._id);
          this.fightersDetailsMap.set(t_fighter._id, t_fighter);
        }
      else{
          t_fighter = this.fightersDetailsMap.get(fighter._id);
      } 
     
      let fighter_id  = document.getElementById("fighter_id") as HTMLInputElement;
      let fighter_name = document.getElementById("fighter_name") as HTMLInputElement;
      let fighter_source = document.getElementById("fighter_source") as HTMLInputElement;

      let fighter_attack = document.getElementById("fighter_attack") as HTMLInputElement;
      let fighter_defense = document.getElementById("fighter_defense") as HTMLInputElement;
      let fighter_health = document.getElementById("fighter_health") as HTMLInputElement;

      fighter_id.value = t_fighter._id.toString();
      fighter_name.value = t_fighter.name;
      fighter_source.value = fighter.source;

      fighter_attack.value = t_fighter.attack.toString();
      fighter_defense.value = t_fighter.defense.toString();
      fighter_health.value = t_fighter.health.toString();
  }

  async editFighter(event):Promise<void>{
    let card = document.getElementById("fighter-card-info");

    let fighter_id  = document.getElementById("fighter_id") as HTMLInputElement;
    let fighter_name = document.getElementById("fighter_name") as HTMLInputElement;
    let fighter_source = document.getElementById("fighter_source") as HTMLInputElement;

    let fighter_attack = document.getElementById("fighter_attack") as HTMLInputElement;
    let fighter_defense = document.getElementById("fighter_defense") as HTMLInputElement;
    let fighter_health = document.getElementById("fighter_health") as HTMLInputElement;

    card.style.display="none";
  
    let t_fighter:Fighter;
    let id = +fighter_id.value;

    if(this.fightersDetailsMap.get(id.toString())==null)
        {
          t_fighter = await fighterService.getFighterDetails(id);
          this.fightersDetailsMap.set(t_fighter._id.toString(), t_fighter);
        }
      else{
          t_fighter = this.fightersDetailsMap.get(id.toString());
      } 

      console.log(t_fighter._id);
    t_fighter.name = fighter_name.value;
    t_fighter.health = +fighter_health.value;
    t_fighter.attack = +fighter_attack.value;
    t_fighter.defense = +fighter_defense.value;
    t_fighter.source = fighter_source.value;

    console.log(t_fighter);
    this.fightersDetailsMap.set(t_fighter._id.toString(), t_fighter);

  }

  async fight(event):Promise<void>{ 
    let checkedCheckbox:number[] = []

    let allInputs = document.getElementsByTagName("input");

    for (var i = 0, max = allInputs.length; i < max; i++){
          if (allInputs[i].type === 'checkbox')
            if(allInputs[i].checked)
              checkedCheckbox.push(+allInputs[i].id);
    }
      if(checkedCheckbox.length!=2)
        alert('Select exactly 2 fighters!')
      else{
        
        let ft1 = this.fightersDetailsMap.get(checkedCheckbox[0].toString());
        let ft2 = this.fightersDetailsMap.get(checkedCheckbox[1].toString());
        
        if(ft1==undefined)
          ft1 = await fighterService.getFighterDetails(checkedCheckbox[0]);
        if(ft2==undefined)
          ft2 = await fighterService.getFighterDetails(checkedCheckbox[1]);
        this.performFight(ft1,ft2);
      }
  }
  
  performFight(fighter1:Fighter, fighter2:Fighter):void{

    console.log(fighter1.health);
    console.log(fighter2.health);

    //doesn`t work without this manipulation)
    let ft1 = new Fighter(fighter1._id, fighter1.name, fighter1.attack, fighter1.defense, fighter1.health, fighter1.source);
    let ft2 = new Fighter(fighter2._id, fighter2.name, fighter2.attack, fighter2.defense, fighter2.health, fighter2.source);
    //
    //hear will be code to display game screen above main
    let game_btn_start = document.getElementById('start');
    game_btn_start.style.display = 'none';

    let game_field = document.getElementById('game-screen');
    game_field.style.display = 'block';

    document.getElementById('f-1-f').appendChild(this.CreateFighterPicture(ft1.source,'f-img-1'));
    document.getElementById('f-2-f').appendChild(this.CreateFighterPicture(ft2.source,'f-img-2'));
    //
    function interval()
    {
    if(ft1.health<=0 || ft2.health<=0){
      if(ft1.health<=0)
        alert(`${ft2.name} won`)
      else 
        alert(`${ft1.name} won`)

      console.log('finished');
      clearInterval(intervalID);
      
      game_field.style.display = 'none';
      game_btn_start.style.display = 'block';
      document.getElementById('f-1-f').removeChild(document.getElementById('f-1-f').firstChild);
      document.getElementById('f-2-f').removeChild(document.getElementById('f-2-f').firstChild);
      document.getElementById('f-1').innerHTML = '';
      document.getElementById('f-2').innerHTML = '';

    }
      else{

        let kick1 = ft2.getHitPower() - ft1.getBlockPower();
        let kick2 = ft1.getHitPower() - ft2.getBlockPower();

        if(kick1<=0)
          ft1.health = ft1.health;
        else 
          ft1.health = ft1.health - kick1;
        if(kick2<=0)
          ft1.health = ft1.health;
        else 
          ft2.health = ft2.health - kick2;

        //
        document.getElementById('f-1').innerHTML = `<h1>${ft1.name}: ${ft1.health.toString()}</h1>`;
        document.getElementById('f-2').innerHTML = `<h1>${ft2.name}: ${ft2.health.toString()}</h1>`;
        //
        console.log(`${ft1.name}: ${ft1.health}; ${ft2.name}: ${ft2.health}`);
      }
    }
    var intervalID=setInterval(interval,700);
    }

    CreateFighterPicture(source:string, className:string):HTMLElement{
      let elem = document.createElement("img");
      elem.setAttribute("src", source);
      elem.className = className;
      return elem;
    }
  }


export default FightersView;