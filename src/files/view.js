"use strict";
exports.__esModule = true;
var View = /** @class */ (function () {
    function View() {
    }
    View.prototype.createElement = function (_a) {
        var tagName = _a.tagName, _b = _a.className, className = _b === void 0 ? '' : _b, _c = _a.attributes, attributes = _c === void 0 ? {} : _c;
        var element = document.createElement(tagName);
        element.classList.add(className);
        Object.keys(attributes).forEach(function (key) { return element.setAttribute(key, attributes[key]); });
        return element;
    };
    return View;
}());
exports["default"] = View;
