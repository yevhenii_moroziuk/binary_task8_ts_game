import View from './view';
import {Fighterlite} from './models/Fighterlite';

class FighterCheckView extends View {
    constructor(fighters:Array<Fighterlite>) {
    super();

    this.createCheckbox(fighters)
  }

  createCheckbox(fighters:Array<Fighterlite>){
    this.element = this.createElement({tagName:'div', className:'check-div'})

    fighters.forEach(fighter => {
    const { name, _id } = fighter;
    let checkBoxElement = document.createElement('input');
    let labelForCheck = document.createElement("label");

    checkBoxElement.type = 'checkbox';
    checkBoxElement.name = name;
    checkBoxElement.id = _id;
    checkBoxElement.className = 'select-fighter';
    
    labelForCheck.htmlFor = _id;
    labelForCheck.innerHTML = name;

    this.element.appendChild(checkBoxElement);
    this.element.appendChild(labelForCheck);
    });
  }
}

export default FighterCheckView;