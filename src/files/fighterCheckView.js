"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var view_1 = require("./view");
var FighterCheckView = /** @class */ (function (_super) {
    __extends(FighterCheckView, _super);
    function FighterCheckView(fighters) {
        var _this = _super.call(this) || this;
        _this.createCheckbox(fighters);
        return _this;
    }
    FighterCheckView.prototype.createCheckbox = function (fighters) {
        var _this = this;
        this.element = this.createElement({ tagName: 'div', className: 'check-div' });
        fighters.forEach(function (fighter) {
            var name = fighter.name, _id = fighter._id;
            var checkBoxElement = document.createElement('input');
            var labelForCheck = document.createElement("label");
            checkBoxElement.type = 'checkbox';
            checkBoxElement.name = name;
            checkBoxElement.id = _id;
            checkBoxElement.className = 'select-fighter';
            labelForCheck.htmlFor = _id;
            labelForCheck.innerHTML = name;
            _this.element.appendChild(checkBoxElement);
            _this.element.appendChild(labelForCheck);
        });
    };
    return FighterCheckView;
}(view_1["default"]));
exports["default"] = FighterCheckView;
