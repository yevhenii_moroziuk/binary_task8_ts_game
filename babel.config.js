var presets = [
    [
        "@babel/preset-env",
        {
            targets: {
                firefox: "60",
                chrome: "67"
            }
        }
    ]
];
var plugins = ["@babel/plugin-proposal-class-properties"];
module.exports = {
    presets: presets,
    plugins: plugins
};
